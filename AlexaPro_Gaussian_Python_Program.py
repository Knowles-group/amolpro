import matplotlib.pyplot as plt
import numpy as np
from pysjef import Project
import pysjef_molpro
from scipy import constants


def absorption_plot():
    # Manually adjust the area of the spectrum to be plotted and the number of points used to plot the curves
    start = 250
    finish = 600
    points = 300

    # Choose a standard deviation value for the Gaussian function
    stdev = 14000

    # Manually insert vertical absorption energies in nm, these are computed by Molpro's TD-DFT program. Values are available in the 'out' file within the project folder.
    bands = []
    # Manually insert oscillator strengths (dimensionless), also computed by the TD-DFT program.
    f = []

    # Basic check that we have the same number of bands and oscillator strengths
    if len(bands) != len(f):
        print('Number of bands does not match the number of oscillator strengths.')
        quit()

    # Generate the absorption spectrum using the matplotlib and numpy graphing software. Construct a Gaussian curve for each peak.
    x = np.linspace(start, finish, points)
    composite = 0
    for count, band in enumerate(bands):
        osc_strength = f[count]
        this_peak = 1.3062974e8 * (osc_strength / (1e7 / stdev)) * np.exp(-(((1.0 / x) - (1.0 / band)) / (1.0 / stdev)) ** 2)
        composite += this_peak

    fig,ax = plt.subplots()
    ax.plot(x,composite)
    plt.xlabel("$\lambda$ / nm")
    plt.ylabel("$\epsilon$ / Lmol$^{-1}$cm$^{-1}$")
    plt.show()

    # Find the wavelength of the most intense absorption band and the type of electromagnetic radiation it corresponds to
    largest_f = max(f)
    index = f.index(largest_f)
    wavelength = int(bands[index])
    dictionary = {(100, 280): 'UV-C radiation', (281, 315): 'UV-B radiation', (316, 400): 'UV-A radiation',
                  (401, 435): 'Purple light', (436, 480): 'Blue light', (481, 490): 'Light blue light',
                  (491, 500): 'Blue-green light', (501, 560): 'Green light', (561, 580): 'Yellow-green light',
                  (581, 595): 'Yellow light', (596, 610): 'Orange light', (611, 750): 'Red light',
                  (751, 800): 'Red-purple light', (801, 1400): 'IR-A radiation', (1401, 3000): 'IR-B radiation',
                  (3001, 1000000): 'IR-C radiation'}

    for key in dictionary:
        if wavelength in range(key[0], key[1]):
            message = dictionary[key]

    print('The wavelength value of the most intense absorption band is {} nm, corresponding to {}'.format(wavelength, message))


def ir_plot():
    pi = constants.pi

    # Manually adjust the area of the spectrum to be plotted and the number of points used to plot the curves
    start = 400
    finish = 3500
    points = 300

    # Choose a Full Width at Half Maximum (FWHM) value (in inverse centimetre) for the Lorentzian function
    fwhm = 20

    name = input('Which project would you like to construct the IR spectrum for? ')
    project = Project("{}.molpro".format(name))

    # XML processing commands to extract the vibrational frequencies (in wavenumbers) and their IR intensities (in units of km/mol) from the output XML file
    vibrations_list = project.select('//vibrations/normalCoordinate.wavenumber')
    vibrations = list(filter((0.0).__ne__, vibrations_list))
    intensities_list = project.select('//vibrations/normalCoordinate[wavenumber].IRintensity')

    for vib in vibrations:
        pos = vibrations.index(vib)
        if intensities_list[pos] == 0.0:
            del vibrations[pos]
    intensities = list(filter((0.0).__ne__, intensities_list))

    # Basic check that we have the same number of IR frequencies and IR intensities
    if len(vibrations) != len(intensities):
        print('Number of vibrational frequencies does not match the number of IR intensities.')
        quit()

    # Generate the IR spectrum using the matplotlib and numpy graphing software. Construct a Lorentzian curve for each peak.
    x = np.linspace(start, finish, points)
    composite = 0
    for count, vib in enumerate(vibrations):
        ir_intensity = intensities[count]
        this_peak = (2 / pi) * (43.42944819 * ir_intensity) * (fwhm / ((4 * ((x - vib) ** 2)) + fwhm ** 2))
        composite += this_peak

    fig, ax = plt.subplots()
    ax.plot(x, composite)
    # Invert the x- and y-axis to represent the IR spectrum
    ax.invert_xaxis()
    ax.invert_yaxis()
    plt.xlabel("Wavenumber / cm$^{-1}$")
    plt.ylabel("$\epsilon$ / Lmol$^{-1}$cm$^{-1}$")
    plt.xlim(3500, 400)
    plt.show()


print('Welcome to the AlexaPro Gaussian Python Program! This piece of software will help you to construct and visualise absorption and IR spectra.')
print('Select one of the following options:')
print('1 - Construct an absorption spectrum from a TD-DFT Molpro calculation.')
print('2 - Construct an IR spectrum from an Optimisation & Frequency Molpro calculation.')
option = input('What mode would you like to use >> ')
print()

if option == "1":
    absorption_plot()
elif option == "2":
    ir_plot()
else:
    print('Please choose an option between 1 and 2.')
    quit()