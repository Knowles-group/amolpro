from pysjef import Project, DirectoryNode
from pysjef_molpro import no_errors
from chemspipy import ChemSpider
from openbabel import pybel

reactant_list = ['dinitrogen', 'dihydrogen']
product_list = ['ammonia']
stoichiometries = ['1', '3', '2']
energy_list = []
reactant_stoichiometries = stoichiometries[:len(reactant_list)]
product_stoichiometries = stoichiometries[len(reactant_list):]

for i in range (len(reactant_list)):
    r = Project("{}.molpro".format(reactant_list[i]))
    energy_r = r.select('//property[name=Energy].value')
    if type(energy_r) is list:
        last_energy = energy_r.pop()
        print(last_energy)
        print(reactant_stoichiometries[i])
        multiply = last_energy * (- int(reactant_stoichiometries[i]))
    energy_list.append(multiply)
    print('Energy for {} = {}eV'.format(reactant_list[i], (-multiply)))

for i in range(len(product_list)):
    p = Project("{}.molpro".format(product_list[i]))
    energy_p = p.select('//property[name=Energy].value')
    if type(energy_p) is list:
        last_energy = energy_p.pop()
        print(last_energy)
        print(product_stoichiometries[i])
        multiply = last_energy * int(product_stoichiometries[i])
    energy_list.append(multiply)
    print('Energy for {} = {}eV'.format(product_list[i], multiply))

energy_difference = sum(energy_list)
deltaE_in_joules = energy_difference * 2625.5

print('Energy difference --> {}eV ; Molar energy change --> {}kJ/mol'.format(energy_difference, deltaE_in_joules))


#stoichiometries = '132'
#reactant_list = ['dinitrogen', 'dihydrogen']
#product_list = ['ammonia']
#energy_list = []

#if str(stoichiometries) == "None":
    #for i in range(len(reactant_list)):
        #r = Project("{}.molpro".format(reactant_list[i]))
        #energy_r = (r.select('//property[name=Energy].value')).pop()
        #energy_list.append(-1 * energy_r)

    #for i in range(len(product_list)):
        #p = Project("{}.molpro".format(product_list[i]))
        #energy_p = (p.select('//property[name=Energy].value')).pop()
        #energy_list.append(energy_p)

#else:
    #stoichiometries_list = list(stoichiometries)
    #reactant_stoichiometries = stoichiometries[:len(reactant_list)]
    #product_stoichiometries = stoichiometries[len(reactant_list):]

    #for i in range(len(reactant_list)):
        #r = Project("{}.molpro".format(reactant_list[i]))
        #energy_r = (r.select('//property[name=Energy].value')).pop()
        #multiply = energy_r * (-1 * int(reactant_stoichiometries[i]))
        #energy_list.append(multiply)

    #for i in range(len(product_list)):
        #p = Project("{}.molpro".format(product_list[i]))
        #energy_p = (p.select('//property[name=Energy].value')).pop()
        #multiply = energy_p * int(product_stoichiometries[i])
        #energy_list.append(multiply)
#print(energy_list)
#energy_difference = sum(energy_list)
#deltaE_in_joules = energy_difference * 2625.5

#print("Energy difference>> {} eV ; Molar energy change>> {}".format(energy_difference, deltaE_in_joules))
