import os
import json
import logging
import numpy
import matplotlib.pyplot as plt

from pysjef import Project, DirectoryNode
from pysjef_molpro import no_errors

root = DirectoryNode('/Users/teresa/forTeresa/MolproCalculations')
p1 = root.add_child('dihydrogen', suffix='molpro')

if p1.completed():
    print("The job is finished")
else:
    print("The job is still running?")
    # alexa should now prompt the user to kill the current job if it is taking too long to finish