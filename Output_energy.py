import os
import json
import logging
import numpy
import matplotlib.pyplot as plt

from pysjef import Project, DirectoryNode
from pysjef_molpro import no_errors
import numpy

property = 'energy'

root = DirectoryNode('/Users/teresa/forTeresa/MolproCalculations')
p1 = root.add_child('dihydrogen', suffix='molpro')

print('Energy=',p1.select('//property[name={}].value'.format(property)))
