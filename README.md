In order to install the voice-IVA system from scratch and launch the AlexaPro skill in your personal computer, please follow the instructions below:
- Download the **_amolpro_** backend code and AlexaPro’s interaction model from the amolpro GiLab repository: https://gitlab.com/Knowles-group/amolpro 
- The files are under the following names: `amolpro.py` (backend function) and `InteractionModel.json`.
- Go to the Amazon Developer’s Console website and open a new, free account: https://developer.amazon.com/alexa/console/ask 
- Once you are logged in, click the Create Skill icon on the screen. Under Skill Name, enter ‘AlexaPro’ and choose your default language. Then, choose the ‘Custom’ skill model and when selecting the method to host the skill’s backend resource, choose the ‘Provision your own’ option.
- When prompted about the template to add to the custom skill, choose the ’Start from Scratch’ option. Creating the skill will take a few seconds.
- Now that the custom skill has been set-up, go to the ‘Build’ section of your skill. On the left-hand pane there will be a menu, choose the ‘Interaction Model’ option and a drop down menu will appear. Go to the ‘JSON Editor’, then drag and drop the `InteractionModel.json` file into the editor. Now click 'Save Model' followed by 'Build Model' at the top of the page. The AlexaPro interaction model is now installed in your custom skill.
- Go back to the ‘Build’ section of your skill and on the left-hand pane, choose the ‘Invocation’ option. Check that the Skill Invocation Name is set to ‘a molpro’, as two separate lowercase words.
- Next, download NGROK, a general port tunneller, on your computer: https://ngrok.com
- Once it is downloaded, run NGROK in a terminal window. Firstly, go to the directory in your computer where ngrok is saved and then launch the application:
  - `cd Downloads` Include the _location in your directory_ in this command. Normally, it is the Downloads folder.
  - `./ngrok http 5000`
- The new terminal window will display two forwarding links, copy the HTTPS URL provided. Then, go back to the Amazon Developer’s Console and at the bottom of the left-hand pane, choose the ‘Endpoint’ option.
- Go down to the bottom of this page and select HTTPS as the Service Endpoint Type for your Alexa skill. Once selected, paste the HTTPS link provided by NGROK in the Default Region box. Then, from the SSL Certificate Type dropdown, choose the second option: `My development endpoint is a sub-domain of a domain that has a wildcard certificate from a certificate authority.`

Now, you have successfully set-up the online AlexaPro endpoint. The next steps involve launching the `amolpro` backend function on your local machine:
- To run the **_amolpro_** code, a Python environment has to be set-up which includes all the essential modules to run the program. Firstly, download the Command Line Installer for the Anaconda environment management system at: https://www.anaconda.com/download/
- The first step is to incorporate the `pysjef` and `pysjef_molpro` modules into the conda environment by cloning the required repositories and running the following script to create and build the environment:
  - `git clone https://gitlab.com/molpro/pysjef.git`
  - `cd pysjef`
  - `bash create_conda_env.sh pysjef`
  - `conda activate pysjef-dev`
  - `git clone https://gitlab.com/molpro/pysjef_molpro.git`
  - `cd pysjef_molpro`
  - `bash build.sh`
- The next step is to install `cmake` and `cython` using pip3:
  - `pip3 install cmake cython`
  - `cd pysjef`
  - `bash build.sh`
- Then, install `flask`, `ask_sdk`, `chemspipy`, `scipy`, `matplotlib`, `numpy` and `openbabel` through a standard pip or conda route. For instance, to download the flask module using pip:
  - `pip3 install flask`
- Now that the environment contains all the required Python modules to run **_amolpro_**, enter the following two commands in the terminal to launch the program: 
  - `export FLASK_APP="amolpro.py"`
  - `flask run`

The **_amolpro_** code should now be running on a terminal, so the offline backend resource is ready. Now, go back to the Amazon Developer’s Console and click on the ‘Test’ section of your skill. Allow the website to use your microphone. At the top of the page, the console informs the user that Test is disabled for this skill, so choose the ‘Development’ option in the dropdown menu. The voice IVA-system is ready to use! Just say (or enter in the chat): ‘Alexa, open a molpro’ and use the AlexaPro skill to operate Molpro.

****************************************************************************************************************************************************
CONTENTS OF THE GITLAB REPOSITORY

- `InteractionModel.json` = JSON file describing the Interaction Model of the AlexaPro skill.
- `amolpro.py` = Final version of the **_amolpro_** backend function for the AlexaPro skill.
- `AlexaPro_Gaussian_Python_Program.py` = Additional program which allows the user to construct and visualise UV/Vis and IR spectra.
- `Demonstration_for_Viva` = mp4 video demonstrating how AlexaPro works.
- `Alexa_calculating_molar_energy_change.py` = COMPLETED/ Code snippet that calculates the molar energy change for the Haber Process reaction. Reactants, products and stoichiometries lists can be edited manually to study a different reaction.
- `calculate_energy_change.py` = COMPLETED/ Code snippet that calculates the molar energy change for any reaction. The program queries the user to obtain a list of reactant and product species, as well as their corresponding stoichiometric factors.
- `Completed_task.py` = COMPLETED/ Code snippet that shows how to use the built-in `project.completed()` pysjef function to check the status of a calculation.
- `Output_energy.py` = COMPLETED/ Code snippet that shows how to use the built-in `project.select()` pysjef function to parse the electronic energy from an ouput XML file.
- `calculate_vibrations_ZPE.py` = COMPLETED/ Code snippet that retrieves all the harmonic vibrational frequencies from a project of the Optimisation and Frequency type. These vibrations are filtered (to remove any imaginary frequencies with a value of zero) and calculates the ZPE value in kJ/mol.
- `comparing_energies.py` = IN PROGRESS / Program that allows the user to compare projects that have different quantum methods or different basis sets. It enables to compare dipole moments or electronic energies of different compounds.
- `diatomic_bond_lenght.py` = COMPLETED / Code snippet that calculates the bond length of a diatomic molecule.
- `dipole_moment_computation.py` = COMPLETED / Code snippet that calculates the dipole moment of a molecule. Firstly, it retrieves the dipole moment vectors for the molecule and converts the values from au to Cm and then from Cm to Debye. The dipole moment vectors are then used to calculate the final value using Pythagora's Theorem.
- `quick_calculation.py` = COMPLETED / Program for the QuickCalculationIntent in the **_amolpro_** code. It allows the user to set up quick Molpro calculations by just entering the name of the compound. If the user wants to improve the calculation parameters, this can be done up to 3 times before the program prompts the user to set up a thorough calculation.
- `retrieving_molecular_formula.py` = COMPLETED / Code snippet that allows the user to query the ChemSpider database for a compound's ChemSpiderID and molecular formula.
- `set_up_Molpro_job.py` = COMPLETED / Program for the SetUpMolproJobIntent in the **_amolpro_** code. It allows the user to set up a thorough Molpro calculation, giving them a choice of calculation type, quantum method, basis set and DFT functional (optional).
- `td-dft_calculation.py` = COMPLETED / Code snippet that includes the recipes to set up TD-DFT calculations in Molpro.
- `test3.py` = COMPLETED / First commited version of the **_amolpro_** backend function.
- `test4.py` = COMPLETED / Second commited version of the **_amolpro_** backend function.
- `test5.py` = COMPLETED / Third commited version of the **_amolpro_** backend function.
- `amolpro1.py` = COMPLETED / Fourth commited version of the **_amolpro_** backend function.
