# -*- coding: utf-8 -*-
# This is version 3 for this project, aiming to implement a few functionalitites listed on the Project Report

import os
import pysjef
import json
import logging
import numpy
import math
import matplotlib.pyplot as plt
import time

from flask import Flask, request, jsonify
from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_core.utils import is_request_type, is_intent_name
from ask_sdk_core.handler_input import HandlerInput

from ask_sdk_model.ui import SimpleCard
from ask_sdk_model import Response, RequestEnvelope

from pysjef import Project, DirectoryNode, all_completed
from pysjef_molpro import no_errors

from chemspipy import ChemSpider
from openbabel import pybel
from scipy import constants

app = Flask(__name__)
cs = ChemSpider('XP5vhxvBu5iC6qqoUZyf1LFUvk8oauxB')

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

sb = SkillBuilder()
handler = sb.lambda_handler()


def current_project(index):
    path = pysjef.recent_project('molpro',index)
    name = str(os.path.splitext(path))
    p = Project(name, suffix='molpro')
    return p
    

def get_project_name():
    index = 1
    path = pysjef.recent_project('molpro',index)
    fullname = str(os.path.basename(path))
    name = fullname.rstrip('.molpro')
    return name
    
    
def status_check(p):
    status = p.status
    dct1 = {'unknown':'has not being found', 'running':'is still running', 'waiting':'is waiting in the queue', 'completed':'is finished', 'unevaluated':'is currently unevaluated'}
    return dct1[status]
    

def request_list(index):
    listf = []

    for i in range(index):
        path = pysjef.recent_project('molpro',index)
        fullname = str(os.path.basename(path))
        listf.append(fullname.rstrip('.molpro'))
        index = index - 1
        
    listf.reverse()
    out_list = ", ".join(listf)
    return out_list
    

def get3Dgeometry(chemID):
    info = cs.get_details(chemID)
    geometry_3D = info['mol3D']           #obtain the 3D geometry of a molecule as a string from ChemSpider
    molecule = pybel.readstring('sdf', geometry_3D)    #pybel reads the molecule from the mol3D string
    str_xyz = molecule.write('XYZ')                        #pybel converts the molecule to an xyz string
    return str_xyz


def set_up_job(name, str_xyz, basis_set, method, optg, freq):
    root = DirectoryNode('.')
    project = root.add_child(name, suffix='molpro')
    project.write_file('input_structure.xyz', str_xyz)
    if optg == "yes":
            if freq == "yes":
                project.write_input('geometry=input_structure.xyz\nbasis={}\n{}\noptg\nput,xyz,input_structure_opt.xyz,new\nfreq'.format(basis_set, method))
            else:
                project.write_input('geometry=input_structure.xyz\nbasis={}\n{}\noptg\nput,xyz,input_structure_opt.xyz,new'.format(basis_set, method))
    else:
        project.write_input('geometry=input_structure.xyz\nbasis={}\n{}'.format(basis_set, method))
    project.run()


def check_results(name, method):
    project = Project(name, suffix='molpro')
    while project.status == 'running':
        time.sleep(0.1)

    rhf_energy = project.select('//property[name=energy, method=RHF].value')
    method_energy = project.select(f'//property[name=Energy, method={method}].value')
    total_energy = project.select(f'//property[name=total energy, method={method}].value')
    dipole_values = project.select('//property[name=Dipole moment].value').pop()
    dipole = calculate_dipole_moment(dipole_values)
    return rhf_energy, method_energy, total_energy, dipole
    
    
def calculate_dipole_moment(dipole_values):
    dipole_list = []
    au_to_Cm = constants.physical_constants['atomic unit of electric dipole mom.'][0]
    Cm_to_Debye = (constants.physical_constants['hertz-inverse meter relationship'][0]) * 1e-21
    
    for i in dipole_values:
        value = (i * au_to_Cm)/Cm_to_Debye
        final = value ** 2
        dipole_list.append(final)
    dipole = math.sqrt(sum(dipole_list))
    return dipole


def default_parameters(i=[-1]):
    basis_list = ['avdz+d', '6-31G*', '6-31G*']
    method_list = ['hf\nmp2', 'hf\nccsd(t)', 'rks,B3LYP']
    i[0] += 1
    return basis_list[i[0]], method_list[i[0]], i[0]


def getZPE(compound):
    planck_constant = constants.h / 1000
    speed_light = constants.c * 100
    avogadro = constants.Avogadro
    ZPE_list = []
    
    project = Project("{}.molpro".format(compound))
    vibrations_list = project.select('//vibrations/normalCoordinate.wavenumber')

    if all(vib == 0.0 for vib in vibrations_list) or vibrations_list == 'None':
        speech_text = "Harmonic vibrational frequencies for {} were not computed by Molpro, as the calculation is not of the optimisation and frequency type.".format(compound)
    else:
        for vib in vibrations_list:
            contribution = vib * planck_constant * speed_light * avogadro
            ZPE_list.append(contribution)
        ZPE = (sum(ZPE_list) / 2).__round__(5)
        speech_text = "The Zero-Point Energy value for {} is {} kJ/mol. To obtain the list of computed vibrational frequencies, please say 'access the IR frequency list'.".format(compound, ZPE)
    return speech_text, vibrations_list
    
    
@sb.request_handler(can_handle_func=is_request_type("LaunchRequest"))
def launch_request_handler(handler_input):
    """Handler for Skill Launch."""
    # type: (HandlerInput) -> Response
    
    session_attributes = handler_input.attributes_manager.session_attributes
    project_name = get_project_name()
    handler_input.attributes_manager.session_attributes["project_name"] = project_name
    
    speech_text = "Welcome to the AlexaPro skill, the voice-automated tool for Molpro!"

    return handler_input.response_builder.speak(speech_text).set_card(
        SimpleCard("Hello World", speech_text)).set_should_end_session(
        False).response

@sb.request_handler(can_handle_func=is_intent_name("CreateProjectIntent"))
def create_project_intent_handler(handler_input):
    """Handler for Create Project Intent."""
    # type: (HandlerInput) -> Response
    
    project_name = handler_input.request_envelope.request.intent.slots['project_name'].value
    project = pysjef.Project(project_name, suffix='molpro')
    
    speech_text = "{} project folder has been created".format(project_name)
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("OpenProjectIntent"))
def open_project_intent_handler(handler_input):
    """Handler for Open Project Intent."""
    # type: (HandlerInput) -> Response
    
    project_name = handler_input.request_envelope.request.intent.slots['project_name'].value
    property = handler_input.request_envelope.request.intent.slots['property'].value
    method = handler_input.request_envelope.request.intent.slots['method'].value
    project = pysjef.Project(project_name, suffix='molpro')

    if property == 'dipole moment' or property == 'dipole':
        dipole_values = project.select('//property[name=Dipole moment].value').pop()
        dipole = calculate_dipole_moment(dipole_values)
        if dipole == 0 or dipole == 0.0:
            answer = str(dipole.__round__(5)) + " Debye. This molecule is non-polar."
        else:
            answer = str(dipole.__round__(5)) + " Debye. This molecule is polar."
        
    elif property == 'electronic energy' or property == 'energy' or property == 'method energy':
        energy = project.select(f'//property[name=Energy, method={method}].value')
        if energy == []:
            energy = project.select(f'//property[name=total energy, method={method}].value')
        answer = str(energy.pop().__round__(5)) + " au"
    
    speech_text = "{} output file has been opened. The {} is {}".format(project_name.capitalize(), property, answer)
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("StatusIntent"))
def status_intent_handler(handler_input):
    """Handler for Status Intent."""
    # type: (HandlerInput) -> Response
    
    molecule = handler_input.request_envelope.request.intent.slots['molecule'].value
    
    if str(molecule) == "None":
        project_name = handler_input.attributes_manager.session_attributes["project_name"]
        index = 1
        project = current_project(index)
        speech_text = '{} project {}'.format(project_name, status_check(project))
    
    else:
        project = pysjef.Project(molecule, suffix='molpro')
        speech_text = '{} project {}'.format(molecule, status_check(project))
    
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("LatestProjectIntent"))
def latest_project_intent_handler(handler_input):
    """Handler for Latest Project Intent."""
    # type: (HandlerInput) -> Response
    
    project_name = handler_input.attributes_manager.session_attributes["project_name"]
    speech_text = "The most recent project accessed was {}".format(project_name)

    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response
    
@sb.request_handler(can_handle_func=is_intent_name("ListRecentProjectsIntent"))
def list_recent_projects_handler(handler_input):
    """Handler for List Recent Projects Intent."""
    # type: (HandlerInput) -> Response
    
    index = int(handler_input.request_envelope.request.intent.slots['index'].value)
    
    if index <= 5 and index > 0:
        list_jobs = request_list(index)
        speech_text = "The last {} projects are {}".format(index, list_jobs)
    
    else:
        handler_input.attributes_manager.session_attributes["list_index"] = index
        speech_text = "The project list might be slightly long, it contains {} items. To read it out loud, access the extended list.".format(index)

    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response
    
@sb.request_handler(can_handle_func=is_intent_name("ExtendedListIntent"))
def extended_list_intent_handler(handler_input):
    """Handler for Extended List Intent."""
    # type: (HandlerInput) -> Response
    
    index = handler_input.attributes_manager.session_attributes["list_index"]
    list_jobs = request_list(index)
    speech_text = "Accessed the extended list. The last {} projects are {}".format(index, list_jobs)
    
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("SetUpMolproJobIntent"))
def set_up_Molpro_job_intent_handler(handler_input):
    """Handler for Set Up Molpro Job Intent."""
    # type: (HandlerInput) -> Response
    
    name = handler_input.request_envelope.request.intent.slots['compound_name'].value
    handler_input.attributes_manager.session_attributes["compound_name"] = name
    results = cs.search(name)
    i = 0
    dict_IDs = {}
    
    for id in results:
        compound = cs.get_compound(id.record_id)     #record the ChemSpiderID for each possible result
        info = compound.molecular_formula            #record the molecular formula for each possible result
        dict_IDs[id.record_id] = info                #save the ChemSpiderID and the molecular formula for each resilt as a key:value pair in dict_IDs
        i += 1
        
    if i == 1:
        chemID = id.record_id
        str_xyz = get3Dgeometry(chemID)
        handler_input.attributes_manager.session_attributes["3D_geometry"] = str_xyz
        speech_text = "The molecular geometry of {} has been obtained from ChemSpider. One search result has been found with molecular formula ({}). Which type of calculation would you like to perform, Single Point Energy, Geometry Optimisation or Optimisation and Frequency?".format(name, dict_IDs[chemID])
    else:
        speech_text = "The molecular geometry of {} has been obtained from ChemSpider. {} search results have been found>> {}. Which ChemSpider ID would you like to access?".format(name, len(results), dict_IDs)
    
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("ChooseChemSpiderIDIntent"))
def choose_ChemSpiderID_intent_handler(handler_input):
    """Handler for Choose ChemSpiderID Intent."""
    # type: (HandlerInput) -> Response
    
    chemID = handler_input.request_envelope.request.intent.slots['id'].value
    str_xyz = get3Dgeometry(chemID)
    handler_input.attributes_manager.session_attributes["3D_geometry"] = str_xyz
    speech_text = "ChemSpiderID ({}) has been selected. Which type of calculation would you like to perform, Single Point Energy, Geometry Optimisation or Optimisation and Frequency?".format(chemID)
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("CalculationTypeIntent"))
def calculation_type_intent_handler(handler_input):
    """Handler for Calculation Type Intent."""
    # type: (HandlerInput) -> Response
    
    type = handler_input.request_envelope.request.intent.slots['calculation_type'].value
    type.lower()
    
    if type == "single point energy":
        optg = "no"
        freq = "no"
    elif type == "geometry optimisation" or type == "geometry optimization":
        optg = "yes"
        freq = "no"
    elif type == "optimisation and frequency" or type == "optimization and frequency" or type == "optimisation/frequency" or type == "optimization/frequency":
        optg = "yes"
        freq = "yes"
    
    handler_input.attributes_manager.session_attributes["optimisation"] = optg
    handler_input.attributes_manager.session_attributes["frequency"] = freq
    speech_text = "Molpro supports quantum methods like Hartree-Fock, DFT, Coupled-Cluster, Møller–Plesset perturbation theory and many more."
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response
    
@sb.request_handler(can_handle_func=is_intent_name("LevelOfTheoryIntent"))
def level_of_theory_intent_handler(handler_input):
    """Handler for Level Of Theory Intent."""
    # type: (HandlerInput) -> Response
    
    theory = handler_input.request_envelope.request.intent.slots['level_of_theory'].value
    theory.lower()
    
    if theory == "hartree fock":
        speech_text = "Choose a command from the following. hf, rhf or uhf."
    elif theory == "dft":
        speech_text = "Choose a command from the following. ks (Kohn-Sham), rks or uks."
    elif theory == "coupled cluster":
        speech_text = "Choose a Coupled-Cluster method. ccsd (Coupled-Cluster with Singles and Doubles), bccd (Brueckner Coupled Cluster Doubles) or qcisd (Quadratic Configuration Interaction with Singles and Doubles)?"
    elif theory == "moller plesset":
        speech_text = "Choose the order of MP theory. Second-Order (mp2), Third-Order (mp3) or Fourth-Order (mp4)?"
    else:
        speech_text = "Other advanced methods include fci (Full Configuration Interaction) and Local Møller–Plesset perturbation theory such as lmp2, lmp3 and lmp4 methods."
    
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("ChooseMethodIntent"))
def choose_method_intent_handler(handler_input):
    """Handler for Choose Method Intent."""
    # type: (HandlerInput) -> Response
    
    method_selected = handler_input.request_envelope.request.intent.slots['final_method'].value
    method_list = ['hf','rhf','uhf','ks','rks','uks']
    mp2_list = ['second-order', 'second order', 'Second-Order', 'Second Order']
    mp3_list = ['third-order', 'third order', 'Third-Order', 'Third Order', '3rd order']
    mp4_list = ['fourth-order', 'fourth order', 'Fourth-Order', 'Fourth Order', '4th order']
    advanced_method_list = ['ccsd','bccd','qcisd','fci', 'lmp2', 'lmp3', 'lmp4']
    
    if method_selected in method_list:
        if method_selected == 'rks' or method_selected == 'uks' or method_selected == 'ks':
               handler_input.attributes_manager.session_attributes["method"] = method_selected
               speech_text = "{} method requires a DFT functional. Please enter a functional command like b3lyp, lda or pbe.".format(method_selected)
        else:
            handler_input.attributes_manager.session_attributes["method"] = method_selected
            speech_text = "{} method has been selected. Which basis set family would you like to use, Dunning, Ahlrichs or Pople?".format(method_selected)
            
    elif method_selected in mp2_list:
        full_method = 'hf\nmp2'
        handler_input.attributes_manager.session_attributes["method"] = full_method
        speech_text = "MP2 method has been selected. Which basis set family would you like to use, Dunning, Ahlrichs or Pople?"
    
    elif method_selected in mp3_list:
        full_method = 'hf\nmp3'
        handler_input.attributes_manager.session_attributes["method"] = full_method
        speech_text = "MP3 method has been selected. Which basis set family would you like to use, Dunning, Ahlrichs or Pople?"
    
    elif method_selected in mp4_list:
        full_method = 'hf\nmp4'
        handler_input.attributes_manager.session_attributes["method"] = full_method
        speech_text = "MP4 method has been selected. Which basis set family would you like to use, Dunning, Ahlrichs or Pople?"
        
    elif method_selected in advanced_method_list:
        if method_selected == "ccsd":
            handler_input.attributes_manager.session_attributes["method"] = method_selected
            speech_text = "The {} method can be improved by adding an f12 correction, adding perturbative triple excitations or both. Which one would you like to add? If you want to use {} only, please say 'no treatment'.".format(method_selected, method_selected)
        elif method_selected == "bccd" or method_selected == "qcisd":
            handler_input.attributes_manager.session_attributes["method"] = method_selected
            speech_text = "To improve the {} method by adding a perturbative treatment, say 'triple excitations'. If not, please say 'no treatment'".format(method_selected)
        else:
            full_method = 'hf\n{}'.format(method_selected)
            handler_input.attributes_manager.session_attributes["method"] = full_method
            speech_text = "{} method has been selected. Which basis set family would you like to use, Dunning, Ahlrichs or Pople?".format(method_selected)
    else:
        speech_text = "This method is unavailable. Please enter a method command again."
   
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("ChooseFunctionalIntent"))
def choose_functional_intent_handler(handler_input):
    """Handler for Choose Functional Intent."""
    # type: (HandlerInput) -> Response
    
    functional = handler_input.request_envelope.request.intent.slots['functional'].value
    functional.lower()
    functional_list = ['b3lyp','B3LYP','b3lyp3','B3LYP3','blyp','lda','pbe','pbe0', 'PBE0', 's','S','b3lyp5','B3LYP5','br','brueg','bw','cs','cs1','CS1','cs2','CS2']
    method_selected = handler_input.attributes_manager.session_attributes["method"]
    
    if functional in functional_list:
        full_method = '{},{}'.format(method_selected,functional)
        handler_input.attributes_manager.session_attributes["method"] = full_method
        speech_text = "{} functional has been selected. Which basis set family would you like to use, Dunning, Ahlrichs or Pople?".format(functional)
    elif functional == "B. 88" or functional == "b88":
        full_method = '{},b88'.format(method_selected)
        handler_input.attributes_manager.session_attributes["method"] = full_method
        speech_text = "b88 functional has been selected. Which basis set family would you like to use, Dunning, Ahlrichs or Pople?"
    elif functional == "B. 86" or functional == "b86":
        full_method = '{},b86'.format(method_selected)
        handler_input.attributes_manager.session_attributes["method"] = full_method
        speech_text = "b86 functional has been selected. Which basis set family would you like to use, Dunning, Ahlrichs or Pople?"
    elif functional == "BP 86" or functional == "bp86":
        full_method = '{},bp86'.format(method_selected)
        handler_input.attributes_manager.session_attributes["method"] = full_method
        speech_text = "bp86 functional has been selected. Which basis set family would you like to use, Dunning, Ahlrichs or Pople?"
    else:
        speech_text = "This DFT functional is unavailable. Please enter a functional command again."
   
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("CoupledClusterIntent"))
def coupled_cluster_intent_handler(handler_input):
    """Handler for Coupled Cluster Intent."""
    # type: (HandlerInput) -> Response
    
    treatment = handler_input.request_envelope.request.intent.slots['treatment'].value
    treatment.lower()
    method_selected = handler_input.attributes_manager.session_attributes["method"]
    
    if treatment == "both" or treatment == "both treatments":
        full_method = 'hf\n{}(t)-f12'.format(method_selected)
    elif treatment == "none" or treatment == "no" or treatment == "no treatment":
        full_method = 'hf\n{}'.format(method_selected)
    elif treatment == "f12 correction" or treatment == "f. twelve correction" or treatment == "F 12 correction" or treatment == "f 12 correction":
        full_method = 'hf\n{}-f12'.format(method_selected)
    elif treatment == "triple excitations":
        full_method = 'hf\n{}(t)'.format(method_selected)
    
    output_method = full_method.split('\n')[1]
    handler_input.attributes_manager.session_attributes["method"] = full_method
    speech_text = "{} method has been selected. Which basis set family would you like to use, Dunning, Ahlrichs or Pople?".format(output_method)
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("ChooseBasisFamilyIntent"))
def choose_basis_family_intent_handler(handler_input):
    """Handler for Choose Basis Family Intent."""
    # type: (HandlerInput) -> Response
    
    basis_family = handler_input.request_envelope.request.intent.slots['basis_family'].value
    
    if basis_family == "Dunning" or basis_family == "dunning":
        speech_text = "Choose a Dunning basis set by indicating the type of basis (standard or tight-d augmented) followed by the basis set of your choice (like vdz or avdz)."
    elif basis_family == "Pople" or basis_family == "pople":
        speech_text = "Choose a Pople basis set from the following. Double Zeta (6-31G*), Polarised Double Zeta (6-31G**), Triple Zeta 6-311G(2DF,2PD) or Augmented Triple Zeta 6-311G++(2DF,2PD)."
    elif basis_family == "Ahlrichs" or basis_family == "ahlrichs":
        speech_text = "Choose a basis set from the following. SVP, TZVP, QZVP, TZVPP or QZVPP."
    else:
        speech_text = "This basis set family is unavailable. Please choose either Dunning, Ahlrichs or Pople."
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response
    
@sb.request_handler(can_handle_func=is_intent_name("BasisSetIntent"))
def basis_set_intent_handler(handler_input):
    """Handler for Basis Set Intent."""
    # type: (HandlerInput) -> Response
    
    basis_set = handler_input.request_envelope.request.intent.slots['basis_set'].value
    type = handler_input.request_envelope.request.intent.slots['type'].value
    Dunning_type = ['standard', 'tight-d augmented', 'tight D augmented']
    Dunning_basis = ['vdz','avdz','vtz','avtz','vqz','avqz','v5z','av5z']
    Advanced_Dunning = ['cvdz','acvdz','wcvdz','awcvdz','cvtz','acvtz','wcvtz','awcvtz','cvqz','acvqz','wcvqz','awcvqz','cv5z','acv5z','wcv5z','awcv5z']
    Ahlrichs_basis = ['SVP','TZVP','QZVP','TZVPP','QZVPP','svp','tzvp','qzvp','tzvpp','qzvpp']
    Pople_basis = ['double zeta', 'Double Zeta', 'polarised double zeta', 'polarized double zeta', 'Polarized Double Zeta', 'Polarised Double Zeta', 'triple zeta', 'Triple Zeta', 'augmented triple zeta', 'Augmented Triple Zeta']
    
    name = handler_input.attributes_manager.session_attributes["compound_name"]
    method_selected = handler_input.attributes_manager.session_attributes["method"]
    optg = handler_input.attributes_manager.session_attributes["optimisation"]
    freq = handler_input.attributes_manager.session_attributes["frequency"]
    
    if basis_set in Ahlrichs_basis:
        handler_input.attributes_manager.session_attributes["basis_set"] = basis_set
        speech_text = "These are the input parameters selected for {}. Method: {}, Basis set: {}, Geometry Optimisation: {}, Optimisation and Frequency: {}. If you are happy with these parameters, please say 'proceed', otherwise please say 'terminate' to cancel the job.".format(name, method_selected, basis_set, optg, freq)
        
    elif basis_set in Pople_basis:
        if basis_set == "double zeta" or basis_set == "Double Zeta":
            basis_set = "6-31G*"
        elif basis_set == "polarised double zeta" or basis_set == "Polarised Double Zeta" or basis_set == "polarized double zeta" or basis_set == "Polarized Double Zeta":
            basis_set = "6-31G**"
        elif basis_set == "triple zeta" or basis_set == "Triple Zeta":
            basis_set = "6-311G(2DF,2PD)"
        else:
            basis_set = "6-311G++(2DF,2PD)"
        handler_input.attributes_manager.session_attributes["basis_set"] = basis_set
        speech_text = "These are the input parameters selected for {}. Method: {}, Basis set: {}, Geometry Optimisation: {}, Optimisation and Frequency: {}. If you are happy with these parameters, please say 'proceed', otherwise please say 'terminate' to cancel the job.".format(name, method_selected, basis_set, optg, freq)
    
    elif type in Dunning_type and basis_set in Dunning_basis:
        if type == "standard":
            handler_input.attributes_manager.session_attributes["basis_set"] = basis_set
        elif type == "tight-d augmented" or type == "tight D augmented":
            basis_set = "{}+d".format(basis_set)
            handler_input.attributes_manager.session_attributes["basis_set"] = basis_set
        speech_text = "These are the input parameters selected for {}. Method: {}, Basis set: {}, Geometry Optimisation: {}, Optimisation and Frequency: {}. If you are happy with these parameters, please say 'proceed', otherwise please say 'terminate' to cancel the job.".format(name, method_selected, basis_set, optg, freq)
    
    elif type in Dunning_type and basis_set in Advanced_Dunning:
        if type == "standard":
            handler_input.attributes_manager.session_attributes["basis_set"] = basis_set
            speech_text = "These are the input parameters selected for {}. Method: {}, Basis set: {}, Geometry Optimisation: {}, Optimisation and Frequency: {}. If you are happy with these parameters, please say 'proceed', otherwise please say 'terminate' to cancel the job.".format(name, method_selected, basis_set, optg, freq)
        elif type == "tight-d augmented" or type == "type D augmented":
            speech_text = "The tight-d augmented version of {} is not available. Please try again.".format(basis_set)
    else:
        speech_text = "This basis set is unavailable. Please try again."

    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("FinalDecisionIntent"))
def final_decision_intent_handler(handler_input):
    """Handler for Final Decision Intent Intent."""
    # type: (HandlerInput) -> Response
    
    decision = handler_input.request_envelope.request.intent.slots['decision'].value
    
    if decision == "proceed" or decision == "Proceed":
        name = handler_input.attributes_manager.session_attributes["compound_name"]
        str_xyz = handler_input.attributes_manager.session_attributes["3D_geometry"]
        method_selected = handler_input.attributes_manager.session_attributes["method"]
        basis_set = handler_input.attributes_manager.session_attributes["basis_set"]
        optg = handler_input.attributes_manager.session_attributes["optimisation"]
        freq = handler_input.attributes_manager.session_attributes["frequency"]
        set_up_job(name, str_xyz, basis_set, method_selected, optg, freq)
        speech_text = "Molpro job for {} has been successfully set up.".format(name)
    else:
        speech_text = "The Molpro job has been terminated."

    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("QuickCalculationIntent"))
def quick_calculation_intent_handler(handler_input):
    """Handler for Quick Calculation Intent."""
    # type: (HandlerInput) -> Response
    
    name = handler_input.request_envelope.request.intent.slots['molecule'].value
    result = cs.search(name)
    dict_IDs = {}
    
    for id in result:
        compound = cs.get_compound(id.record_id)
        info = compound.molecular_formula
        dict_IDs[id.record_id] = info

    chemID = next(iter(dict_IDs))
    str_xyz = get3Dgeometry(chemID)
    handler_input.attributes_manager.session_attributes["compound_name"] = name
    handler_input.attributes_manager.session_attributes["3D_geometry"] = str_xyz
    default_method = 'hf\nmp2'
    method_selected = default_method.split('\n')[1].upper()
    default_basis = 'vdz'
    optg = "yes"
    freq = "no"
    set_up_job(name, str_xyz, default_basis, default_method, optg, freq)
    parameters = check_results(name, method_selected)
    
    if (parameters[0] == []) and (parameters[2] == []):
        speech_text = "A quick geometry optimisation has been performed for {} using the {} method and the {} basis set. Output results include>> {} Energy: {}au and Dipole moment: {}D. To use a more accurate method and basis set, please say 'improve default input parameters'.".format(name, method_selected, default_basis, method_selected, parameters[1].pop().__round__(5), parameters[3].__round__(5))
    elif (parameters[1] == []) and (parameters[2] == []):
        speech_text = "A quick geometry optimisation has been performed for {} using the {} method and the {} basis set. Output results include>> RHF Energy: {}au and Dipole moment: {}D. To use a more accurate method and basis set, please say 'improve default input parameters'.".format(name, method_selected, default_basis, parameters[0].pop().__round__(5), parameters[3].__round__(5))
    elif not parameters[1]:
        speech_text = "A quick geometry optimisation has been performed for {} using the {} method and the {} basis set. Output results include>> RHF Energy: {}au, {} Energy: {}au and Dipole moment: {}D. To use a more accurate method and basis set, please say 'improve default input parameters'.".format(name, method_selected, default_basis, parameters[0].pop().__round__(5), method_selected, parameters[2].pop().__round__(5), parameters[3].__round__(5))
    elif not parameters[2]:
        speech_text = "A quick geometry optimisation has been performed for {} using the {} method and the {} basis set. Output results include>> RHF Energy: {}au, {} Energy: {}au and Dipole moment: {}D. To use a more accurate method and basis set, please say 'improve default input parameters'.".format(name, method_selected, default_basis, parameters[0].pop().__round__(5), method_selected, parameters[1].pop().__round__(5), parameters[3].__round__(5))
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("ChangeDefaultSettingsIntent"))
def change_default_settings_intent_handler(handler_input):
    """Handler for Change Default Settings Intent."""
    # type: (HandlerInput) -> Response
    
    name = handler_input.attributes_manager.session_attributes["compound_name"]
    str_xyz = handler_input.attributes_manager.session_attributes["3D_geometry"]
    defaults = default_parameters()
    optg = "yes"
    freq = "no"
    set_up_job(name, str_xyz, defaults[0], defaults[1], optg, freq)
    
    if ',' in defaults[1]:
        method = defaults[1].split(",")[0].upper()
    else:
        method = defaults[1].split("\n")[1].upper()
    parameters = check_results(name, method)
    
    if (parameters[0] == []) and (parameters[2] == []):
        first_speech = "Default input parameters have been changed for {} and a geometry optimisation has been carried out. The new basis set is {} and the new method is {}. Output results include>> {} Energy: {}au and Dipole moment: {}D.".format(name, defaults[0], method, method, parameters[1].pop().__round__(5), parameters[3].__round__(5))
    elif not parameters[1]:
        first_speech = "Default input parameters have been changed for {} and a geometry optimisation has been carried out. The new basis set is {} and the new method is {}. Output results include>> RHF Energy: {}au, {} Energy: {}au and Dipole moment: {}D.".format(name, defaults[0], method, parameters[0].pop().__round__(5), method, parameters[2].pop().__round__(5), parameters[3].__round__(5))
    elif not parameters[2]:
        first_speech = "Default input parameters have been changed for {} and a geometry optimisation has been carried out. The new basis set is {} and the new method is {}. Output results include>> RHF Energy: {}au, {} Energy: {}au and Dipole moment: {}D.".format(name, defaults[0], method, parameters[0].pop().__round__(5), method, parameters[1].pop().__round__(5), parameters[3].__round__(5))
    
    if defaults[2] < 2:
        speech_text = first_speech + " To use another set of more accurate parameters, please say 'improve default parameters' again."
    elif defaults[2] == 2:
        speech_text = first_speech + " If you wish to perform another calculation, I recommend setting up a new Molpro job with me as I can provide more input parameter options. Please say 'set up ... Molpro job', including the name of the molecule of interest."
            
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("CalculateZPEIntent"))
def calculate_ZPE_intent_handler(handler_input):
    """Handler for Calculate ZPE Intent."""
    # type: (HandlerInput) -> Response
    
    molecule = handler_input.request_envelope.request.intent.slots['molecule'].value
    handler_input.attributes_manager.session_attributes["IR_molecule"] = molecule
    
    if str(molecule) == "None":
        molecule = handler_input.attributes_manager.session_attributes["project_name"]
        parameters = getZPE(molecule)
        speech_text = "{}".format(parameters[0])
    else:
        parameters = getZPE(molecule)
        speech_text = "{}".format(parameters[0])
    
    handler_input.attributes_manager.session_attributes["IR_frequencies"] = parameters[1]
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("IRIntent"))
def ir_intent_handler(handler_input):
    """Handler for IR Intent."""
    # type: (HandlerInput) -> Response
    
    molecule = handler_input.attributes_manager.session_attributes["IR_molecule"]
    project = pysjef.Project(molecule, suffix='molpro')
    
    vibrations = handler_input.attributes_manager.session_attributes["IR_frequencies"]
    vibrations = list(filter((0.0).__ne__,vibrations))
    intensities_list = project.select('//vibrations/normalCoordinate[wavenumber].IRintensity')
    for vib in vibrations:
        pos = vibrations.index(vib)
        if intensities_list[pos] == 0.0:
            del vibrations[pos]
    list_to_str = ', '.join([str(vib) for vib in vibrations])
    
    speech_text = "These are the computed vibrational frequencies (in wavenumbers) for {}: {}. View the output IR spectrum in Gnuplot or download the AlexaPro Gaussian Python program.".format(molecule, list_to_str)
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("GetBondLengthIntent"))
def get_bond_length_intent_handler(handler_input):
    """Handler for Get Bond Length Intent."""
    # type: (HandlerInput) -> Response
    
    diatomic = handler_input.request_envelope.request.intent.slots['diatomic_name'].value
    project = pysjef.Project(diatomic, suffix='molpro')
    bond_length = project.select('//*[id=a1].x3').pop()
    if bond_length == '0.0':
        bond_length = project.select('//*[id=a1].z3').pop()
        total_bond_length = bond_length * -2
    else:
        total_bond_length = bond_length * 2
    
    speech_text = "The bond length for {} is {} Angstroms".format(diatomic, total_bond_length.__round__(5))

    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response
    
@sb.request_handler(can_handle_func=is_intent_name("StartMolarEnergyIntent"))
def start_molar_energy_intent_handler(handler_input):
    """Handler for Start Molar Energy Intent."""
    # type: (HandlerInput) -> Response
    
    speech_text = "To calculate the molar energy change of a reaction, enter a list of all the reactant species involved."
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response
    
@sb.request_handler(can_handle_func=is_intent_name("GetReactantsIntent"))
def get_reactants_intent_handler(handler_input):
    """Handler for Get Reactants Intent."""
    # type: (HandlerInput) -> Response
    
    reactants = handler_input.request_envelope.request.intent.slots['reactants'].value
    reactant_list = reactants.split()
    handler_input.attributes_manager.session_attributes["reactant_list"] = reactant_list
    speech_text = "Next, enter a list of all the product species in the reaction."
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("GetProductsIntent"))
def get_products_intent_handler(handler_input):
    """Handler for Get Products Intent."""
    # type: (HandlerInput) -> Response
    
    products = handler_input.request_envelope.request.intent.slots['products'].value
    product_list = products.split()
    handler_input.attributes_manager.session_attributes["product_list"] = product_list
    speech_text = "Now, list the stoichiometric factors of the species involved. If the reaction has a 1:1 stochiometry, then say 'all stoichiometries are one'."
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response
    
@sb.request_handler(can_handle_func=is_intent_name("GetStoichiometriesIntent"))
def get_products_intent_handler(handler_input):
    """Handler for Get Products Intent."""
    # type: (HandlerInput) -> Response
    
    stoichiometries = handler_input.request_envelope.request.intent.slots['stoichiometries'].value
    reactant_list = handler_input.attributes_manager.session_attributes["reactant_list"]
    product_list = handler_input.attributes_manager.session_attributes["product_list"]
    energy_list = []
    
    if str(stoichiometries) == "None":
        for i in range (len(reactant_list)):
            r = pysjef.Project(reactant_list[i], suffix='molpro')
            energy_r = r.select('//property[name=Energy].value').pop()
            energy_list.append(-1 * energy_r)
            
        for i in range(len(product_list)):
            p = pysjef.Project(product_list[i], suffix='molpro')
            energy_p = p.select('//property[name=Energy].value').pop()
            energy_list.append(energy_p)
        
    else:
        stoichiometries_list = list(stoichiometries)
        reactant_stoichiometries = stoichiometries[:len(reactant_list)]
        product_stoichiometries = stoichiometries[len(reactant_list):]
    
        for i in range (len(reactant_list)):
            r = pysjef.Project(reactant_list[i], suffix='molpro')
            energy_r = r.select('//property[name=Energy].value').pop()
            multiply = energy_r * (-1 * int(reactant_stoichiometries[i]))
            energy_list.append(multiply)

        for i in range(len(product_list)):
            p = pysjef.Project(product_list[i], suffix='molpro')
            energy_p = p.select('//property[name=Energy].value').pop()
            multiply = energy_p * int(product_stoichiometries[i])
            energy_list.append(multiply)

    energy_difference = sum(energy_list)
    deltaE_in_joules = energy_difference * 2625.5

    speech_text = "Energy difference>> {} au ; Molar energy change>> {} kJ/mol".format(energy_difference.__round__(5), deltaE_in_joules.__round__(5))
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("TimeDependentDFTIntent"))
def time_dependent_dft_intent_handler(handler_input):
    """Handler for Time Dependent DFT Intent."""
    # type: (HandlerInput) -> Response
    
    molecule = handler_input.request_envelope.request.intent.slots['molecule'].value
    excited_states = handler_input.request_envelope.request.intent.slots['number_excited_states'].value
    root = DirectoryNode('.')
    project = root.add_child(molecule, suffix='molpro')
    results = cs.search(molecule)
    dict_IDs = {}
    
    for id in results:
        compound = cs.get_compound(id.record_id)
        info = compound.molecular_formula
        dict_IDs[id.record_id] = info

    chemID = next(iter(dict_IDs))
    str_xyz = get3Dgeometry(chemID)
    project.write_file('input_structure.xyz', str_xyz)
    
    if str(excited_states) == "None":
        project.write_input('symmetry,nosym\ngeometry=input_structure.xyz\nbasis=vdz\nks,pbe0\ntddft,states=[10.1];gnuplot,spec.gp')
    else:
        project.write_input(f'symmetry,nosym\ngeometry=input_structure.xyz\nbasis=vdz\nks,pbe0\ntddft,states=[{excited_states}.1];gnuplot,spec.gp')
    project.run()
    speech_text = "A TD-DFT calculation for {} has been carried out using the PBE0 functional and the cc-pVDZ basis set. View the output absorption spectrum in Gnuplot or download the AlexaPro Gaussian Python program.".format(molecule)
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("AMAZON.HelpIntent"))
def help_intent_handler(handler_input):
    """Handler for Help Intent."""
    # type: (HandlerInput) -> Response
    speech_text = "AlexaPro can help you set up a Molpro job, perform a quick quantum calculation, find the bond length for a diatomic molecule, calculate the ZPE, compute the molar energy change for a chemical reaction and much more!"

    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).response

@sb.request_handler(can_handle_func=lambda handler_input:is_intent_name("AMAZON.CancelIntent")(handler_input) or is_intent_name("AMAZON.StopIntent")(handler_input))
def cancel_and_stop_intent_handler(handler_input):
    """Single handler for Cancel and Stop Intent."""
    # type: (HandlerInput) -> Response
    
    speech_text = "Thank you for using the AlexaPro skill!"
    return handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).response

@sb.request_handler(can_handle_func=is_intent_name("AMAZON.FallbackIntent"))
def fallback_handler(handler_input):
    # type: (HandlerInput) -> Response
    
    speech_text = "Sorry, I don't understand that. Please try again."
    return handler_input.response_builder.speak(speech_text).response

@sb.exception_handler(can_handle_func=lambda i, e: True)
def all_exception_handler(handler_input, exception):
    # type: (HandlerInput, Exception) -> Response
    logger.error(exception, exc_info=True)

    speech_text = "Sorry, there was some problem. Please try again."
    return handler_input.response_builder.speak(speech_text).response

@app.route('/', methods=['POST'])
def post():
    """
    Process the request as following :
    - Get the input request JSON
    - Deserialize it to Request Envelope
    - Verify the request was sent by Alexa
    - Invoke the skill
    - Return the serialized response
    """

    content = request.json
    request_envelope = skill_obj.serializer.deserialize(
        payload=json.dumps(content), obj_type=RequestEnvelope)

    response_envelope = skill_obj.invoke(
        request_envelope=request_envelope, context=None)
    print(response_envelope)
    return jsonify(skill_obj.serializer.serialize(response_envelope))


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')


skill_obj = sb.create()
