from pysjef import Project, DirectoryNode
from pysjef_molpro import no_errors
from chemspipy import ChemSpider
from openbabel import pybel

cs = ChemSpider('XP5vhxvBu5iC6qqoUZyf1LFUvk8oauxB')
root = DirectoryNode('.')

#********************************************************************************************************************
print('This program determines the electronic energies of reactants and products and')
print('consequently, calculates the molar energy change.')
print()
reactant_dict = {}
product_dict = {}

answer = 'y'
while answer == 'y':
    reactant = input('Enter the name of the file of the reactant species>> ')
    stoichiometry_r = float(input('What is the stoichiometry factor of this reactant species>> '))
    reactant_dict[reactant] = stoichiometry_r
    answer = input('Is there another reactant present in the reaction?[y/n]')

print()
answer = 'y'
while answer == 'y':
    product = input('Enter the name of the file of the product species>> ')
    stoichiometry_p = float(input('What is the stoichiometry factor of this product species>> '))
    product_dict[product] = stoichiometry_p
    answer = input('Is there another product generated from this reaction?[y/n]')
#********************************************************************************************************************
print()
print('*****************************************************************************************************')
energy_list = []

for reactant in reactant_dict:
    r = Project("{}.molpro".format(reactant))
    energy_r = r.select('//property[name=Energy].value')
    if type(energy_r) is list:
        last_energy = energy_r.pop()
        multiply = last_energy * (- reactant_dict[reactant])
    energy_list.append(multiply)
    print('Energy for {} = {}eV'.format(reactant, (-multiply)))

for product in product_dict:
    p = Project("{}.molpro".format(product))
    energy_p = p.select('//property[name=Energy].value')
    if type(energy_p) is list:
        last_energy = energy_p.pop()
        multiply = last_energy * product_dict[product]
    energy_list.append(multiply)
    print('Energy for {} = {}eV'.format(product, multiply))

energy_difference = sum(energy_list)
deltaE_in_joules = energy_difference * 2625.5

print('Energy difference --> {}eV ; Molar energy change --> {}kJ/mol'.format(energy_difference, deltaE_in_joules))
print('*****************************************************************************************************')
