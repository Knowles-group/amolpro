from pysjef import Project, DirectoryNode
from pysjef_molpro import no_errors
from chemspipy import ChemSpider
from openbabel import pybel
from scipy import constants
import math

cs = ChemSpider('XP5vhxvBu5iC6qqoUZyf1LFUvk8oauxB')
root = DirectoryNode('.')
planck_constant = constants.h / 1000
speed_light = constants.c * 100
avogadro = constants.Avogadro
ZPE_list = []

#********************************************************************************************************************
print('This program determines the harmonic vibrational frequencies for a molecule of interest')
print('in wavenumbers and calculates the zero point energy (ZPE) in kJ/mol.')
print()

molecule = input('Enter the name of the file of the molecule of interest>> ')
m = Project("{}.molpro".format(molecule))
vibrations_m = m.select('//vibrations/normalCoordinate.wavenumber')
print()

if all(vib == 0.0 for vib in vibrations_m):
    print('Harmonic vibrational frequencies for {} were not computed by Molpro, as the calculation is not of the '
          'opt+frequency type.'.format(molecule))
    quit()
else:
    vibrations_m = list(filter((0.0).__ne__,vibrations_m))
    list_to_str = ', '.join([str(vib) for vib in vibrations_m])
    print('These are the harmonic vibrational frequencies (in wavenumbers) for {}:'.format(molecule))
    print(list_to_str)
    print()

    for vib in vibrations_m:
        contribution = vib * planck_constant * speed_light * avogadro
        ZPE_list.append(contribution)

    ZPE = (sum(ZPE_list) / 2).__round__(5)
    print('Zero-point Energy (ZPE) for {} >> {} kJ/mol'.format(molecule, ZPE))