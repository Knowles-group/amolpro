from pysjef import Project, DirectoryNode
from pysjef_molpro import no_errors
from chemspipy import ChemSpider
from openbabel import pybel
import matplotlib.pyplot as plt
import numpy as np

cs = ChemSpider('XP5vhxvBu5iC6qqoUZyf1LFUvk8oauxB')


def energies_diff_compounds():
    answer = 'y'
    i = 1
    mol_direc_dict = {}
    energy_list = []
    name_method_dict = {}

    while answer == 'y':
        molecule = input('Enter project {}>> '.format(i))
        directory = input('In which directory is the project located? Please provide the full path>> ')
        i = i + 1
        mol_direc_dict[molecule] = directory
        answer = input('Is there another project you want to open?[y/n]')

    print()
    print('*****************************************************')
    for molecule in mol_direc_dict:
        root = mol_direc_dict[molecule]
        m = Project("{}.molpro".format(molecule))

        energy_m = m.select('//property[name=Energy].value')  # obtains energy value
        if type(energy_m) is list:
            last_energy = energy_m.pop()
            energy_list.append(last_energy)

        method = m.select('//property[name=Energy].method')  # obtains method
        if type(method) is list:
            last_method = method.pop()
            name_method_dict[molecule] = last_method

        input_list = m.select(
            '//input/p')  # obtaining array with all input parameters for the job. Currently all properties are parsed as xml objects, so it's not possible to discern between the basis and the functional
        print(input_list)
        basis = input_list[1]  # obtains basis set from the xml object array

        print(molecule.upper())
        print('Method: {} / Basis: {} / Energy: {}eV'.format(last_method, basis, last_energy))
        if last_method == 'RKS' or last_method == 'UKS' or last_method == 'KS':
            print('DFT functional: {}'.format(input_list[2]))
    print('*****************************************************')
    print()

    x = np.array(range(i-1))
    y = np.array(energy_list)
    my_xticks = []
    for molecule in name_method_dict:
        my_xticks.append(molecule.capitalize() + ' / ' + name_method_dict[molecule])
    plt.xticks(x, my_xticks)
    plt.xticks(rotation=45)
    plt.xlabel('Molecules')
    plt.ylabel('Energy /eV')
    plt.title("Comparing Electronic Correlation Energies")
    plt.plot(x, y, '-bo')
    for i_x, i_y in zip(x, y):
        plt.text(i_x, i_y,'({}, {})'.format(i_x, i_y))
    plt.show()


def dipoles_diff_compounds():
    answer = 'y'
    i = 1
    mol_direc_dict = {}
    name_method_dict = {}
    dipole_list = []

    while answer == 'y':
        molecule = input('Enter project {}>> '.format(i))
        directory = input('In which directory is the project located? Please provide the full path>> ')
        i = i + 1
        mol_direc_dict[molecule] = directory
        answer = input('Is there another project you want to open?[y/n]')

    print()
    print('*****************************************************')
    for molecule in mol_direc_dict:
        root = mol_direc_dict[molecule]
        m = Project("{}.molpro".format(molecule))

        dipole_m = m.select('//property[name=Dipole moment].value')  # obtains energy value
        if type(dipole_m) is list:
            dipole_list.append(dipole_m[0])

        method = m.select('//property[name=Energy].method')  # obtains method
        if type(method) is list:
            last_method = method.pop()
            name_method_dict[molecule] = last_method

        input_list = m.select('//input/p')  # obtaining array with all input parameters for the job. Currently all properties are parsed as xml objects, so it's not possible to discern between the basis and the functional
        basis = input_list[1]  # obtains basis set from the xml object array

        print(molecule.upper())
        print('Method: {} / Basis: {} / Dipole moment: {}'.format(last_method, basis, dipole_m[0]))
        if last_method == 'RKS' or last_method == 'UKS' or last_method == 'KS':
            print('DFT functional: {}'.format(input_list[2]))
    print('*****************************************************')
    print()
    print(dipole_list)
    x = np.array(range(i-1))
    y = np.array(dipole_list)
    my_xticks = []
    for molecule in mol_direc_dict:
        my_xticks.append(molecule.capitalize())
    plt.xticks(x, my_xticks)
    plt.xticks(rotation=45)
    plt.xlabel('Molecules')
    plt.ylabel('Dipole Moment /D')
    plt.title("Comparing Dipole Moments for Different Compounds")
    plt.plot(x, y, '-bo')
    for i_x, i_y in zip(x, y):
        plt.text(i_x, i_y,'({}, {})'.format(i_x, i_y))
    plt.show()


print('This program investigates electron correlation effects on molar energy changes when different methods or')
print('different basis sets are used. Dipole moments can also be computed and compared for a range of molecules.')
print('********************************************************************')
print('COMPARISON METHODS')
print('Compare energies for different compounds in a reaction - 1')
print('Compare dipole moments for different compounds in a reaction - 2')
print('Compare energies for the same compound but different methods - 3')
print('Compare energies for the same compound but different basis sets - 4')
print('********************************************************************')
print()
choice = input('Select the comparison method you prefer>> ')

if choice == '1':
    energies_diff_compounds()
elif choice == '2':
    dipoles_diff_compounds()
elif choice == '3':
    pass
elif choice == '4':
    pass
