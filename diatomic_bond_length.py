from pysjef import Project, DirectoryNode
import pysjef_molpro
import pysjef
from chemspipy import ChemSpider
from openbabel import pybel
import time
import math


cs = ChemSpider('Xj00cmmQDYftbrLlPCfrQdUvudILkYlG')
project = pysjef.Project('dinitrogen', suffix='molpro')

bond_length = project.select('//*[id=a1].x3').pop()
total_bond_length = bond_length * 2
print(bond_length)
print(total_bond_length.__round__(5))
