from pysjef import Project, DirectoryNode
import pysjef_molpro
import pysjef
from scipy import constants
import math

au_to_Cm = constants.physical_constants['atomic unit of electric dipole mom.'][0]
print(au_to_Cm)
Cm_to_Debye = (constants.physical_constants['hertz-inverse meter relationship'][0]) * 1e-21
print(Cm_to_Debye)

name = 'water'
project = pysjef.Project(name, suffix='molpro')
dipole_values = project.select('//property[name=Dipole moment].value').pop()
dipole_list = []

for i in dipole_values:
    value = (i * au_to_Cm)/Cm_to_Debye
    final = value ** 2
    dipole_list.append(final)
dipole = math.sqrt(sum(dipole_list))

print(dipole)