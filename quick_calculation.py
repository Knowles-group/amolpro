from pysjef import Project, DirectoryNode
import pysjef_molpro
import pysjef
from chemspipy import ChemSpider
from openbabel import pybel
import time
import math


cs = ChemSpider('XP5vhxvBu5iC6qqoUZyf1LFUvk8oauxB')
root = DirectoryNode('.')
name = 'methane'
result = cs.search(name)
dict_IDs = {}


def get3Dgeometry(chemID):
    info = cs.get_details(chemID)
    geometry_3D = info['mol3D']  # obtain the 3D geometry of a molecule as a string from ChemSpider
    molecule = pybel.readstring('sdf', geometry_3D)  # pybel reads the molecule from the mol3D string
    str_xyz = molecule.write('XYZ')  # pybel converts the molecule to an xyz string
    return str_xyz


def set_up_job(name, str_xyz, basis, method):
    project = pysjef.Project(name, suffix='molpro')
    project.write_file('input_structure.xyz', str_xyz)
    project.write_input('geometry=input_structure.xyz\nbasis={}\n{}\noptg\nput,xyz,input_structure_opt.xyz,new'.format(basis, method))
    project.run()


def check_results(name, method):
    project = pysjef.Project(name, suffix='molpro')
    while project.status == 'running':
        time.sleep(0.1)

    rhf_energy = project.select("//property[name=energy,method=RHF].value")
    method_energy = project.select(f'//property[name=energy, method={method}].value')
    total_energy = project.select(f'//property[name=total energy, method={method}].value')
    dipole_values = project.select('//property[name=Dipole moment].value').pop()
    dipole_list = []
    for i in dipole_values:
        value = i ** 2
        dipole_list.append(value)
    dipole = math.sqrt(sum(dipole_list))
    return rhf_energy, method_energy, total_energy, dipole


def default_parameters(i=[-1]):
    basis_list = ['avdz+d', '6-31G*', '6-31G*']
    method_list = ['hf\nmp2', 'hf\nccsd(t)', 'rks,B3LYP']
    i[0] += 1
    return basis_list[i[0]], method_list[i[0]], i[0]


for id in result:
    compound = cs.get_compound(id.record_id)  # record the ChemSpiderID for each possible result
    info = compound.molecular_formula
    dict_IDs[id.record_id] = info

chemID = next(iter(dict_IDs))
str_xyz = get3Dgeometry(chemID)
basis = 'vdz'
method = 'hf\nmp2'
set_up_job(name, str_xyz, basis, method)
method_selected = method.split("\n")[1].upper()
parameters = check_results(name, method_selected)

print('A quick calculation has been performed for {}. Output results include>> RHF Energy: {}eV, {} Energy: {}eV and Dipole moment: {}'.format(name, parameters[0].pop().__round__(5), method_selected, parameters[2].pop().__round__(5), parameters[3]))
option = input('Would you like to improve the default input parameters?[y/n] ')

while option == "y":
    defaults = default_parameters()
    set_up_job(name, str_xyz, defaults[0], defaults[1])
    if ',' in defaults[1]:
        method = defaults[1].split(",")[0].upper()
    else:
        method = defaults[1].split("\n")[1].upper()
    parameters = check_results(name, method)

    if defaults[2] <= 2:
        if (parameters[0] == []) and (parameters[2] == []):
            print('Default input parameters have been changed for {}. New basis set is {} and new method is {}. Output results include>> {} Energy: {}eV and Dipole moment: {}'.format(name, defaults[0], method, method, parameters[1].pop().__round__(5), parameters[3].__round__(14)))
        elif not parameters[1]:
            print('Default input parameters have been changed for {}. New basis set is {} and new method is {}. Output results include>> RHF Energy: {}eV,  {} Energy: {}eV and Dipole moment: {}'.format(name, defaults[0], method, parameters[0].pop().__round__(5), method, parameters[2].pop().__round__(5), parameters[3].__round__(14)))
        elif not parameters[2]:
            print('Default input parameters have been changed for {}. New basis set is {} and new method is {}. Output results include>> RHF Energy: {}eV,  {} Energy: {}eV and Dipole moment: {}'.format(name, defaults[0], method, parameters[0].pop().__round__(5), method, parameters[1].pop().__round__(5), parameters[3].__round__(14)))

    elif defaults[2] == 2:
        print('Better try set up ... Molpro job')
    option = input('Would you like to improve the default input parameters?[y/n] ')
