from pysjef import Project, DirectoryNode
from pysjef_molpro import no_errors
from chemspipy import ChemSpider
from openbabel import pybel
import os

#api_key = os.environ['CHEMSPIDER_API_KEY']
cs = ChemSpider('XP5vhxvBu5iC6qqoUZyf1LFUvk8oauxB')
#cs = ChemSpider(api_key)
name = input('Input the name of the compound of interest>> ')
results = cs.search(name)
i = 0
dict_IDs = {}

for id in results:
    compound = cs.get_compound(id.record_id)
    structure = compound.molecular_formula
    dict_IDs[id.record_id] = structure

print(dict_IDs)
