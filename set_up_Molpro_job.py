from pysjef import Project, DirectoryNode
from pysjef_molpro import no_errors
from chemspipy import ChemSpider
from openbabel import pybel

cs = ChemSpider('Xj00cmmQDYftbrLlPCfrQdUvudILkYlG')
root = DirectoryNode('.')

#***************************************************************************************************************************************
name = input('Input the name of the compound of interest>> ')    #name of compound and method for the calculation are required
method = input('Input the method of choice>> ').lower()
results = cs.search(name)
i = 0
dict_IDs = {}
method_list = ['hf','rhf','uhf','ks','rks','uks']
advanced_method_list = ['mp2','mp3','mp4','ccsd','ccsd(t)','bccd','bccd(t)','qcisd','qcisd(t)','fci','mp2-f12','ccsd-f12','ccsd(t)-f12','ldf-rhf','ldf-uhf','lmp2','pno-lmp2','pno-lmp2-f12','pno-rccsd','pno-rccsd-f12','pno-rccsd(t)','pno-rccsd(t)-f12','pno-uccsd','pno-uccsd-f12','pno-uccsd(t)','pno-uccsd(t)-f12']
basis_list = ['6-31G*','6-31G**','6-311++G(D,P)','VDZ','AVDZ','VDZ+d','AVDZ+d','CVDZ','ACVDZ','WCVDZ','AWCVDZ','VTZ','AVTZ','VTZ+d','AVTZ+d','CVTZ','ACVTZ','WCVTZ','AWCVTZ','VQZ','AVQZ','VQZ+d','AVQZ+d','CVQZ','ACVQZ','WCVQZ','AWCVQZ','V5Z','AV5Z','V5Z+d','AV5Z+d','CV5Z','ACV5Z','WCV5Z','AWCV5Z']
functional_list = ['b3lyp','b3lyp3','blyp','bp86','lda','pbe','pbe0','s','b','b-lyp','b-p','b-vwn','b3lyp5','b86','b86mgc','b86r','b88','b88c','b88x','b95','b97','b97df','b97r','b97rdf','becke','bh-lyp','br','brueg','bw','cs','cs1','cs2','d','dirac']

if method in method_list:                         #check if the method selected is available in Molpro, if not, quit the program
    pass
else:                                             #if a coupled-cluster method is selected, then a HF calculation will be performed first
    if method in advanced_method_list:
        method = 'hf\n{}'.format(method)
    else:
        print('This method is unavailable.')
        quit()

if method == 'rks' or method == 'uks' or method == 'ks':                      #if a DFT calculation is requested, a DFT functional must be entered by the user
    functional = input('Input the DFT functional of choice>> ').lower()       #checking that the DFT functional is available, if not, quit the program
    if functional in functional_list:
        method = '{},{}'.format(method,functional)
    else:
        print('This DFT functional is unavailable')
        quit()

#****************************************************************************************************************************************
basis = input('Input the basis of choice>> ').upper()    #basis for the calculation is required
if basis in basis_list:                          #check if the basis selected is available in Molpro, if not, quit the program
    pass
else:
    print('This basis is unavailable.')
    quit()
#****************************************************************************************************************************************
optg = input('Would you like to perform a geometry optimisation?[y/n] ')
print()

for id in results:
    compound = cs.get_compound(id.record_id)  #record the ChemSpiderID for each possible result
    info = compound.molecular_formula         #record the molecular formula for each possible result
    dict_IDs[id.record_id] = info             #save the ChemSpiderID and the molecular formula for each resilt as a key:value pair in dict_IDs
    i += 1

if i == 1:
    chemID = id.record_id
    print('Only one ChemSpiderID was found. Obtaining the 3D geometry for Compound({}) with geometry>> {}'.format(chemID, dict_IDs[chemID]))
    print()

else:
    print('ChemSpider IDs found, with their respective molecular formula >> {}'.format(dict_IDs))
    chemID = int(input('There are {} possible results. Which ChemSpider ID would you like to access? '.format(len(results))))
    print('Obtaining the 3D geometry for Compound({})'.format(chemID))
    print()

info = cs.get_details(chemID)
geometry_3D = info['mol3D']                           #obtain the 3D geometry of a molecule as a string from ChemSpider
molecule = pybel.readstring('sdf', geometry_3D)       #pybel reads the molecule from the mol3D string
str_xyz = molecule.write('XYZ')                       #pybel converts the molecule to an xyz string
project = root.add_child(name, suffix='molpro')
project.write_file('input_structure.xyz',str_xyz)                           #creates a file called 'input_structure' in the project with the 3D geometry of the molecule as an xyz string

if optg == 'n':
    project.write_input('geometry=input_structure.xyz\nbasis={}\n{}'.format(basis, method))      #sets up the Molpro job input with the geometry file created, the basis set and the method
else:
    freq = input('Would you like to compute the vibrational frequencies?[y/n] ')
    print()
    if freq == 'n':
        project.write_input('geometry=input_structure.xyz\nbasis={}\n{}\noptg\nput,xyz,input_structure_opt.xyz,new'.format(basis, method))      #sets up a Geometry Optimisation Molpro job with the geometry file created, the basis set and the method
    else:
        project.write_input('geometry=input_structure.xyz\nbasis={}\n{}\noptg\nput,xyz,input_structure_opt.xyz,new\nfreq'.format(basis, method))      #sets up an Opt+Frequency Molpro job with the geometry file created, the basis set and the method

# To calculate thermodynamic properties for a molecule of interest, at the end of the job input file (which must be
# computing the vibrational frequencies) you must write \nthermo,sym={} in a new line, after the freq statement. The
# symmetry of the molecule should be requested to the user, as ChemSpider doesn't pass this as an info argument.

project.run()
print('Molpro job for {} has been successfully set up and ran.'.format(name))
