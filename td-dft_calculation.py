from pysjef import Project, DirectoryNode
from pysjef_molpro import no_errors
import pysjef
from chemspipy import ChemSpider
from openbabel import pybel
import matplotlib.pyplot as plt
import numpy as np

cs = ChemSpider('XP5vhxvBu5iC6qqoUZyf1LFUvk8oauxB')
root = DirectoryNode('.')
dict_IDs = {}
name = 'water'
results = cs.search(name)

for id in results:
    compound = cs.get_compound(id.record_id)
    info = compound.molecular_formula
    dict_IDs[id.record_id] = info

chemID = next(iter(dict_IDs))
info = cs.get_details(chemID)
geometry_3D = info['mol3D']
molecule = pybel.readstring('sdf', geometry_3D)
str_xyz = molecule.write('XYZ')
project = root.add_child(name, suffix='molpro')
project.write_file('input_structure.xyz',str_xyz)

#Computing the energies and oscillator frequencies of the first 20 excited states
project.write_input('symmetry,nosym\ngeometry=input_structure.xyz\nbasis=avtz\nks,blyp\ntddft,states=[20.1];gnuplot,<spec.gp>')
project.run()
#Force program provides the same output as prior job AND computes the RKS gradient (F=dE/dr) for state 1.1
project.write_input('symmetry,nosym\ngeometry=input_structure.xyz\nbasis=6-31G*\nks,pbe0\ntddft,states=[20.1]\nforce')
project.run()
#Geometry Optimisation for the first excited state and calculating the TDDFT gradient. Also computes the charge and dipole moments for the ground and first electronic excited state
project.write_input('symmetry,nosym\ngeometry=input_structure.xyz\nbasis=6-31G*\nks,pbe0\ntddft,grad=1,states=[20.1]\noptg')
project.run()
