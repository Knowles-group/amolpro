# -*- coding: utf-8 -*-
# This is version 3 for this project, aiming to implement a few functionalitites listed on the Project Report

import logging
import os
import json
import ask_sdk_core.utils as ask_utils
import numpy
import matplotlib.pyplot as plt

from flask import Flask, request, jsonify
from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_core.utils import is_request_type, is_intent_name
from ask_sdk_core.handler_input import HandlerInput
from ask_sdk_core.dispatch_components import AbstractRequestHandler
from ask_sdk_core.dispatch_components import AbstractExceptionHandler

from ask_sdk_model.ui import SimpleCard
from ask_sdk_model import Response, RequestEnvelope

from pysjef import Project, DirectoryNode, all_completed
from pysjef_molpro import no_errors

app = Flask(__name__)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

sb = SkillBuilder()
lambda_handler = sb.lambda_handler()

class LaunchRequestHandler(AbstractRequestHandler):
    # Handler for the Skill Launch
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        
        return ask_utils.is_request_type("LaunchRequest")(handler_input)
        
    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speech_text = "Welcome to the AlexaMolpro skill"
        
        return handler_input.response_builder.speak(speech_text).set_card(
            SimpleCard("Hello World", speech_text)).set_should_end_session(
            False).response

class CreateProjectIntentHandler(AbstractRequestHandler):
    # Handler for the Create Project Intent
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("CreateProjectIntent")(handler_input)
        
    def handle(self, handler_input):
        speech_text = "Creating a new Molpro project file"
        
        filepath = os.path.join('c:/Users/teresa/forTeresa/newFiles', 'molpro_input')
        if not os.path.exists('c:/Users/teresa/forTeresa/newFiles'):
            os.makedirs('c:/Users/teresa/forTeresa/newFiles')
            
        new_file = open("molpro_input.txt", "w+")
        new_file.close()
        
        return handler_input.response_builder.speak(speech_text).set_card(
            SimpleCard("Hello World", speech_text)).set_should_end_session(
            True).response
        
class RunMolproIntentHandler(AbstractRequestHandler):
    # Handler for the Run Molpro Intent
    def can_handle(self, handler_input):
        return ask_utils.is_intent_name("RunMolproIntent")(handler_input)
        
    def handle(self, handler_input):
        speech_text = "Running a random calculation to test Molpro"
        
        root = DirectoryNode('rundir')
        p1 = root.add_child('p1', suffix='molpro')
        p1.write_input(f"""geometry={{He}};rhf""")
        p1.run(wait=True)
        print('completed', p1.completed())
        # print('without errors', p1.)
        energy_value = p1.select('//property[name=Energy].value')
        print('Energy=' , energy_value)
        print(p1.view_output())
        
        value_output = "The energy for He is {}".format(energy_value)
        
        return handler_input.response_builder.speak(speech_text, value_output).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(
            True).response


sb.add_request_handler(LaunchRequestHandler())
sb.add_request_handler(CreateProjectIntentHandler())
sb.add_request_handler(RunMolproIntentHandler())

if __name__ == '__main__':
    app.run(debug=True)
