# -*- coding: utf-8 -*-
# This is version 3 for this project, aiming to implement a few functionalitites listed on the Project Report

import os
import json
import logging
import numpy
import matplotlib.pyplot as plt

from flask import Flask, request, jsonify
from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_core.utils import is_request_type, is_intent_name
from ask_sdk_core.handler_input import HandlerInput

from ask_sdk_model.ui import SimpleCard
from ask_sdk_model import Response, RequestEnvelope

from pysjef import Project, DirectoryNode, all_completed
from pysjef_molpro import no_errors


app = Flask(__name__)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

sb = SkillBuilder()
handler = sb.lambda_handler()

@sb.request_handler(can_handle_func=is_request_type("LaunchRequest"))
def launch_request_handler(handler_input):
    """Handler for Skill Launch."""
    # type: (HandlerInput) -> Response
    speech_text = "Welcome to the AlexaPro skill, the voice-automated tool for Molpro!"

    return handler_input.response_builder.speak(speech_text).set_card(
        SimpleCard("Hello World", speech_text)).set_should_end_session(
        False).response

@sb.request_handler(can_handle_func=is_intent_name("CreateProjectIntent"))
def create_project_intent_handler(handler_input):
    """Handler for Create Project Intent."""
    # type: (HandlerInput) -> Response
    global p1, root
    
    speech_text = "Creating a new project file..."
    
    slots = handler_input.request_envelope.request.intent.slots
    structure = str(slots['structure'].value)
    file_name = str(slots['file_name'].value)

    root = DirectoryNode('MolproCalculations')
    p1 = root.add_child(file_name, suffix='molpro')
    #p1.write_input(f"""geometry={{"""structure"""}};rhf""")

    ask_question = "What would you like to do next? I can run, copy, delete or rename the file"

    return handler_input.response_builder.speak(speech_text).ask(ask_question).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("RunningMolproIntent"))
def running_molpro_intent_handler(handler_input):
    """Handler for Running Molpro Intent."""
    # type: (HandlerInput) -> Response
    speech_text = "Running Molpro calculation!"

    #root = DirectoryNode('MolproCalculations')
    #p1.run()
    
    return handler_input.response_builder.speak(speech_text).set_card(
        SimpleCard("Hello World", speech_text)).set_should_end_session(
        True).response


@sb.request_handler(can_handle_func=is_intent_name("FinishedIntent"))
def finished_intent_handler(handler_input):
    """Handler for Finished Intent."""
    # type: (HandlerInput) -> Response

    if p1.completed() == True:
        speech_text = "The job is finished"
        
        return handler_input.response_builder.speak(speech_text).set_card(
            SimpleCard("Hello World", speech_text)).set_should_end_session(
            True).response
        
    else:
        speech_text = "The job is still running"
        ask_question = "Would you like to kill the job?"

        return handler_input.response_builder.speak(speech_text).ask(ask_question).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("KillJobIntent"))
def kill_job_intent_handler(handler_input):
    """Handler for Kill Job Intent."""
    # type: (HandlerInput) -> Response
    speech_text = "Killing the job currently running..."
    
    slots = handler_input.request_envelope.request.intent.slots
    file_name = str(slots['file_name'].value)

    #file_name.kill  command to kill the job DirectoryNode
    
    ask_question = "What would you like to do next? I can create, open and run a different file."

    return handler_input.response_builder.speak(speech_text).ask(ask_question).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response


@sb.request_handler(can_handle_func=is_intent_name("OutputIntent"))
def output_intent_handler(handler_input):
    """Handler for Output Intent."""
    # type: (HandlerInput) -> Response
    
    #energy_value = p1.select('//property[name=Energy].value')
    #p1.view_output()

    speech_text = "The energy is {}".format(energy_value)

    return handler_input.response_builder.speak(speech_text).set_card(
        SimpleCard("Hello World", speech_text)).set_should_end_session(
        True).response

@sb.request_handler(can_handle_func=is_intent_name("OpenFileIntent"))
def open_file_intent_handler(handler_input):
    """Handler for Open File Intent."""
    # type: (HandlerInput) -> Response
    speech_text = "Opening the file..."
    
    slots = handler_input.request_envelope.request.intent.slots
    file_name = str(slots['file_name'].value)

    #file_name.  #command to open a file within the DirectoryNode
    
    ask_question = "What would you like to do next? I can run the file, delete, copy or rename it"

    return handler_input.response_builder.speak(speech_text).ask(ask_question).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("DeleteFileIntent"))
def delete_file_intent_handler(handler_input):
    """Handler for Delete File Intent."""
    # type: (HandlerInput) -> Response
    global p1, root
    speech_text = "Deleting the file..."
    
    slots = handler_input.request_envelope.request.intent.slots
    file_name = str(slots['file_name'].value)

    #p1 = root.erase()
    #file_name.erase()  command to delete a file from the DirectoryNode
    
    ask_question = "What would you like to do next? I can create, open and run a different file"

    return handler_input.response_builder.speak(speech_text).ask(ask_question).set_card(SimpleCard("Hello World", speech_text)).response
    
@sb.request_handler(can_handle_func=is_intent_name("CopyFileIntent"))
def copy_file_intent_handler(handler_input):
    """Handler for Copy File Intent."""
    # type: (HandlerInput) -> Response
    speech_text = "Copying the file..."
    
    slots = handler_input.request_envelope.request.intent.slots
    file_name = str(slots['file_name'].value)

    #file_name.copy_node  command to copy a file from the DirectoryNode
    
    ask_question = "What would you like to do next? I can rename, delete or open this file."

    return handler_input.response_builder.speak(speech_text).ask(ask_question).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("RenameFileIntent"))
def rename_file_intent_handler(handler_input):
    """Handler for Rename File Intent."""
    # type: (HandlerInput) -> Response
    speech_text = "Renaming the file..."
    
    slots = handler_input.request_envelope.request.intent.slots
    file_name = str(slots['file_name'].value)

    #file_name.  command to rename a file from the DirectoryNode
    
    ask_question = "What would you like to do next? I can run, copy or delete this file."

    return handler_input.response_builder.speak(speech_text).ask(ask_question).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response


@sb.request_handler(can_handle_func=is_intent_name("AMAZON.HelpIntent"))
def help_intent_handler(handler_input):
    """Handler for Help Intent."""
    # type: (HandlerInput) -> Response
    speech_text = "This is the AlexaPro skill, you can create, open and run quantum chemical calculations"

    return handler_input.response_builder.speak(speech_text).ask(
        speech_text).set_card(SimpleCard(
            "Hello World", speech_text)).response


@sb.request_handler(
    can_handle_func=lambda handler_input:
        is_intent_name("AMAZON.CancelIntent")(handler_input) or
        is_intent_name("AMAZON.StopIntent")(handler_input))
def cancel_and_stop_intent_handler(handler_input):
    """Single handler for Cancel and Stop Intent."""
    # type: (HandlerInput) -> Response
    speech_text = "Thank you for using AlexaPro!"

    return handler_input.response_builder.speak(speech_text).set_card(
        SimpleCard("Hello World", speech_text)).response


@sb.request_handler(can_handle_func=is_intent_name("AMAZON.FallbackIntent"))
def fallback_handler(handler_input):
    """AMAZON.FallbackIntent is only available in en-US locale.
    This handler will not be triggered except in that locale,
    so it is safe to deploy on any locale.
    """
    # type: (HandlerInput) -> Response
    speech = (
        "The Hello World skill can't help you with that.  "
        "You can say hello!!")
    reprompt = "You can say hello!!"
    handler_input.response_builder.speak(speech).ask(reprompt)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_request_type("SessionEndedRequest"))
def session_ended_request_handler(handler_input):
    """Handler for Session End."""
    # type: (HandlerInput) -> Response
    return handler_input.response_builder.response


@sb.exception_handler(can_handle_func=lambda i, e: True)
def all_exception_handler(handler_input, exception):
    """Catch all exception handler, log exception and
    respond with custom message.
    """
    # type: (HandlerInput, Exception) -> Response
    logger.error(exception, exc_info=True)

    speech = "Sorry, there was some problem. Please try again!!"
    handler_input.response_builder.speak(speech).ask(speech)

    return handler_input.response_builder.response


@app.route('/', methods=['POST'])
def post():
    """
    Process the request as following :
    - Get the input request JSON
    - Deserialize it to Request Envelope
    - Verify the request was sent by Alexa
    - Invoke the skill
    - Return the serialized response
    """

    content = request.json
    request_envelope = skill_obj.serializer.deserialize(
        payload=json.dumps(content), obj_type=RequestEnvelope)

    response_envelope = skill_obj.invoke(
        request_envelope=request_envelope, context=None)
    print(response_envelope)
    return jsonify(skill_obj.serializer.serialize(response_envelope))


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')


skill_obj = sb.create()
