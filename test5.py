# -*- coding: utf-8 -*-
# This is version 3 for this project, aiming to implement a few functionalitites listed on the Project Report

import os
import json
import logging
import numpy
import matplotlib.pyplot as plt

from flask import Flask, request, jsonify
from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_core.utils import is_request_type, is_intent_name
from ask_sdk_core.handler_input import HandlerInput

from ask_sdk_model.ui import SimpleCard
from ask_sdk_model import Response, RequestEnvelope

from pysjef import Project, DirectoryNode, all_completed
from pysjef_molpro import no_errors


app = Flask(__name__)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

sb = SkillBuilder()
handler = sb.lambda_handler()

def recent_project():
    return Project(recent_project())

@sb.request_handler(can_handle_func=is_request_type("LaunchRequest"))
def launch_request_handler(handler_input):
    """Handler for Skill Launch."""
    # type: (HandlerInput) -> Response
    speech_text = "Welcome to the AlexaPro skill, the voice-automated tool for Molpro!"

    return handler_input.response_builder.speak(speech_text).set_card(
        SimpleCard("Hello World", speech_text)).set_should_end_session(
        False).response

@sb.request_handler(can_handle_func=is_intent_name("CreateProjectIntent"))
def create_project_intent_handler(handler_input):
    """Handler for Create Project Intent."""
    # type: (HandlerInput) -> Response
    
    speech_text = "Creating a new project file..."
    
    slots = handler_input.request_envelope.request.intent.slots
    file_name = str(slots['file_name'].value)

    root = DirectoryNode('/Users/teresa/forTeresa/MolproCalculations')
    p1 = root.add_child(file_name, suffix='molpro')

    ask_question = "What would you like to do next? I can run, copy, delete or rename the file"

    return handler_input.response_builder.speak(speech_text).ask(ask_question).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response
    
@sb.request_handler(can_handle_func=is_intent_name("WriteInputFileIntent"))
def write_input_file_intent_handler(handler_input):
    """Handler for Write Input File Intent."""
    # type: (HandlerInput) -> Response
    
    slots = handler_input.request_envelope.request.intent.slots
    file_name = str(slots['file_name'].value)
    structure = str(slots['structure'].value)
    method = str(slots['method'].value)
    basis_set = str(slots['basis_set'].value)
    
    p2 = recent_project()
    p2.write_input("""geometry={};{}""".format(structure, method))
    #p1.write_input(f"""geometry={{He}};rhf""")
    #is this syntax correct?
    
    speech_text = "Input file completed!"

    ask_question = "What would you like to do next? I can run, copy, delete or rename the file"

    return handler_input.response_builder.speak(speech_text).ask(ask_question).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("RunningMolproIntent"))
def running_molpro_intent_handler(handler_input):
    """Handler for Running Molpro Intent."""
    # type: (HandlerInput) -> Response
    
    slots = handler_input.request_envelope.request.intent.slots
    file_name = str(slots['file_name'].value)
    
    root = DirectoryNode('/Users/teresa/forTeresa/MolproCalculations')
    p2 = Project("{}.molpro".format(file_name))
    p2.run()
    p2.recent(1)
    
    speech_text = "Running Molpro calculation..." + p2.recent(1)
    
    return handler_input.response_builder.speak(speech_text).set_card(
        SimpleCard("Hello World", speech_text)).set_should_end_session(
        True).response


@sb.request_handler(can_handle_func=is_intent_name("FinishedIntent"))
def finished_intent_handler(handler_input):
    """Handler for Finished Intent."""
    # type: (HandlerInput) -> Response
    
    root = DirectoryNode('/Users/teresa/forTeresa/MolproCalculations')
    p2 = root.add_child('dihydrogen', suffix="molpro")

    if p2.completed():
        speech_text = "The job is finished"
        
        return handler_input.response_builder.speak(speech_text).set_card(
            SimpleCard("Hello World", speech_text)).set_should_end_session(
            True).response
        
    else:
        speech_text = "The job is still running"
        ask_question = "Would you like to kill the job?"

        return handler_input.response_builder.speak(speech_text).ask(ask_question).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("KillJobIntent"))
def kill_job_intent_handler(handler_input):
    """Handler for Kill Job Intent."""
    # type: (HandlerInput) -> Response
    speech_text = "Killing the job currently running..."
    
    slots = handler_input.request_envelope.request.intent.slots
    file_name = str(slots['file_name'].value)

    #file_name.kill  command to kill the job DirectoryNode
    #I don't want to be asking the user for the file name of the job currently running
    #Is there an option to ask Molpro what file it's running?
    
    ask_question = "What would you like to do next? I can create, open and run a different file."

    return handler_input.response_builder.speak(speech_text).ask(ask_question).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response


@sb.request_handler(can_handle_func=is_intent_name("OutputIntent"))
def output_intent_handler(handler_input):
    """Handler for Output Intent."""
    # type: (HandlerInput) -> Response
    
    slots = handler_input.request_envelope.request.intent.slots
    file_name = str(slots['file_name'].value)
    property = str(slots['property'].value)
    
    root = DirectoryNode('/Users/teresa/forTeresa/MolproCalculations')
    p2 = root.add_child(file_name, suffix="molpro")
    
    if slots['property'].value is None:
        p2.view_output()
        speech_text = "Obtaining your results...Please view the Molpro output"
        
    else:
        energy_value = p2.select('//property[name={}].value'.format(property))   #energy in dihydrogen file gives two results, for rhf and mp2
        speech_text = "Obtaining the results from {} file... The {} is {}".format(file_name, property, energy_value)

    return handler_input.response_builder.speak(speech_text).set_card(
        SimpleCard("Hello World", speech_text)).set_should_end_session(
        True).response

@sb.request_handler(can_handle_func=is_intent_name("OpenFileIntent"))
def open_file_intent_handler(handler_input):
    """Handler for Open File Intent."""
    # type: (HandlerInput) -> Response
    speech_text = "Opening the file..."
    
    slots = handler_input.request_envelope.request.intent.slots
    file_name = str(slots['file_name'].value)

    p2 = Project("{}.molpro".format(file_name))
    
    ask_question = "What would you like to do next? I can run the file, delete, copy or rename it"

    return handler_input.response_builder.speak(speech_text).ask(ask_question).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("DeleteFileIntent"))
def delete_file_intent_handler(handler_input):
    """Handler for Delete File Intent."""
    # type: (HandlerInput) -> Response

    speech_text = "Deleting the file..."
    
    slots = handler_input.request_envelope.request.intent.slots
    file_name = str(slots['file_name'].value)
    
    p2 = Project("{}.molpro".format(file_name))

    p2.erase()
    #is this the command to delete a file from the DirectoryNode
    
    ask_question = "What would you like to do next? I can create, open and run a different file"

    return handler_input.response_builder.speak(speech_text).ask(ask_question).set_card(SimpleCard("Hello World", speech_text)).response
    
@sb.request_handler(can_handle_func=is_intent_name("CopyFileIntent"))
def copy_file_intent_handler(handler_input):
    """Handler for Copy File Intent."""
    # type: (HandlerInput) -> Response
    
    slots = handler_input.request_envelope.request.intent.slots
    file_name = str(slots['file_name'].value)
    directory = str(slots['directory'].value)
    
    root = DirectoryNode('MolproCalculations')
    p2 = root.add_child(file_name, "molpro")
    
    if slots['directory'].value is None:
        p2.copy_node()    # is this the correct syntax?
        speech_text = "Copying the file..."
    
    else:
        try:
            p2.copy(file_name, directory)
            speech_text = "Copying the file into {}...".format(directory)
        
        except Exception:
            speech_text = "Sorry, I couldn't find directory requested"
        

    ask_question = "What would you like to do next? I can rename, delete or open this file."

    return handler_input.response_builder.speak(speech_text).ask(ask_question).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response

@sb.request_handler(can_handle_func=is_intent_name("RenameFileIntent"))
def rename_file_intent_handler(handler_input):
    """Handler for Rename File Intent."""
    # type: (HandlerInput) -> Response
    
    root = DirectoryNode('MolproCalculations')
    p2 = root.add_child(file_name, "molpro")
    
    slots = handler_input.request_envelope.request.intent.slots
    file_name = str(slots['file_name'].value)
    new_name = str(slots['new_name'].value)

    file_name_full = file_name + ".molpro"
    new_name_full = new_name + ".molpro"
    os.rename(file_name_full, new_name_full)  #is this command only used for renaming files and not folders?
    
    speech_text = "Renaming the file..."
    
    ask_question = "What would you like to do next? I can run, copy or delete this file."

    return handler_input.response_builder.speak(speech_text).ask(ask_question).set_card(SimpleCard("Hello World", speech_text)).set_should_end_session(False).response


@sb.request_handler(can_handle_func=is_intent_name("AMAZON.HelpIntent"))
def help_intent_handler(handler_input):
    """Handler for Help Intent."""
    # type: (HandlerInput) -> Response
    speech_text = "This is the AlexaPro skill, you can create, open and run quantum chemical calculations"

    return handler_input.response_builder.speak(speech_text).ask(
        speech_text).set_card(SimpleCard(
            "Hello World", speech_text)).response


@sb.request_handler(
    can_handle_func=lambda handler_input:
        is_intent_name("AMAZON.CancelIntent")(handler_input) or
        is_intent_name("AMAZON.StopIntent")(handler_input))
def cancel_and_stop_intent_handler(handler_input):
    """Single handler for Cancel and Stop Intent."""
    # type: (HandlerInput) -> Response
    speech_text = "Thank you for using AlexaPro!"

    return handler_input.response_builder.speak(speech_text).set_card(
        SimpleCard("Hello World", speech_text)).response


@sb.request_handler(can_handle_func=is_intent_name("AMAZON.FallbackIntent"))
def fallback_handler(handler_input):
    """AMAZON.FallbackIntent is only available in en-US locale.
    This handler will not be triggered except in that locale,
    so it is safe to deploy on any locale.
    """
    # type: (HandlerInput) -> Response
    speech = (
        "The Hello World skill can't help you with that.  "
        "You can say hello!!")
    reprompt = "You can say hello!!"
    handler_input.response_builder.speak(speech).ask(reprompt)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_request_type("SessionEndedRequest"))
def session_ended_request_handler(handler_input):
    """Handler for Session End."""
    # type: (HandlerInput) -> Response
    return handler_input.response_builder.response


@sb.exception_handler(can_handle_func=lambda i, e: True)
def all_exception_handler(handler_input, exception):
    """Catch all exception handler, log exception and
    respond with custom message.
    """
    # type: (HandlerInput, Exception) -> Response
    logger.error(exception, exc_info=True)

    speech = "Sorry, there was some problem. Please try again!!"
    handler_input.response_builder.speak(speech).ask(speech)

    return handler_input.response_builder.response


@app.route('/', methods=['POST'])
def post():
    """
    Process the request as following :
    - Get the input request JSON
    - Deserialize it to Request Envelope
    - Verify the request was sent by Alexa
    - Invoke the skill
    - Return the serialized response
    """

    content = request.json
    request_envelope = skill_obj.serializer.deserialize(
        payload=json.dumps(content), obj_type=RequestEnvelope)

    response_envelope = skill_obj.invoke(
        request_envelope=request_envelope, context=None)
    print(response_envelope)
    return jsonify(skill_obj.serializer.serialize(response_envelope))


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')


skill_obj = sb.create()
